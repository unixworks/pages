---
hide:
  - footer
  - toc
  - navigation
---

# BSD as your main OS, in a nutshell.

---

Welcome to BSDworks, formerly known as *unixworks*, a practical approach series of workshops and guides about the power of BSD as a desktop for daily and production usage.

This project is actually being updated an maintained at [vertexfarm](https://vertexfarm.xyz/content/notes/000_index.html). Please refer there to read the latest version of the content.

